import React, { Component } from 'react'

import Main from './components/routes/routes'

class App extends Component {
  render() {
    return <Main />
  }
}

export default App
