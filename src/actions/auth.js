import jwt_decode from 'jwt-decode'
import API from '../config/api'
import setToken from '../config/set_token'
import { GET_ERRORS, SET_CURRENT_USER } from '../actions/types'
import history from '../services/history'

export const registrationDispatch = user => dispatch => {
  console.log(user)
  API.post('/registration/', user)
    .then(res => {
      console.log(res)
      history.push('/login/')
    })
    .catch(err => {
      console.log(err)
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    })
}

export const loginDispatch = user => dispatch => {
  API.post('/login/', user)
    .then(res => {
      console.log(res)
      const { token } = res.data
      localStorage.setItem('jwtToken', `Bearer ${token}`)
      setToken(`Bearer ${token}`)

      const decoded = jwt_decode(token)
      console.log(decoded)
      dispatch(setCurrentUser(decoded))

      history.push('/')
    })
    .catch(err => {
      console.log(err)
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    })
}

export const setCurrentUser = decoded => dispatch => {
  dispatch({
    type: SET_CURRENT_USER,
    payload: decoded
  })
}

export const logoutDispatch = () => dispatch => {
  localStorage.removeItem('jwtToken')
  setToken(false)
  dispatch(setCurrentUser({}))
}
