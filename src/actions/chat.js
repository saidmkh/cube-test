import API from '../config/api'

export const getMessages = () => {
  return API.get('/chat/')
}
