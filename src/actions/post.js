import API from '../config/api'

export const getAllPosts = () => {
  return API.get('/posts/')
}

export const getPost = postId => {
  return API.get(`/posts/${postId}`)
}

export const createPost = post => {
  return API.post(`/post/`, post)
}

export const deletePost = postId => {
  return API.delete(`/post/${postId}`)
}

export const updatePost = (postId, post) => {
  console.log(post)
  return API.put(`/post/${postId}`, post)
}

export const createComment = (postId, content) => {
  return API.post(`/post/${postId}/comment`, { content })
}

export const deleteComment = (postId, commentId) => {
  return API.delete(`/post/${postId}/comment/${commentId}`)
}

export const updateComment = (postId, commentId, content) => {
  return API.put(`/post/${postId}/comment/${commentId}`, { content })
}
