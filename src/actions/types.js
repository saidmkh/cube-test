//------ Auth action types ------
export const SET_CURRENT_USER = 'SET_CURRENT_USER'
export const GET_ERRORS = 'ERRORS'

//------ User action types ------
export const GET_USERS = 'GET_USERS'

//------ Post action types ------
export const GET_POST = 'GET_POST'
