import API from '../config/api'

export const getUsers = users => {
  return API.get('/admin/users/')
}

export const deleteUser = userId => {
  return API.delete(`/user/${userId}`)
}

export const updateUser = (userId, user) => {
  return API.put(`/user/${userId}`, user)
}
