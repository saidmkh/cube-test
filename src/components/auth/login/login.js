import React from 'react'
import { Link } from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

import LoginForm from './login_form'

const styles = theme => ({
  authWrapper: {
    minHeight: '65vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  paper: {
    marginTop: '-64px',
    padding: '1rem'
  },
  navAuth: {
    margin: '1rem 0',
    fontSize: '1rem'
  },
  navLink: {
    color: 'blue',
    fontSize: '1.1rem',
    marginLeft: '3px'
  },
  authBlock: {
    maxWidth: 400,
    width: '100%'
  }
})

const Login = props => {
  const { classes } = props

  return (
    <div className={classes.authWrapper}>
      <div className={classes.authBlock}>
        <Paper className={classes.paper} spacing={16} p={2}>
          <Grid align="center">
            <Typography align="center" variant="h5" component="h2">
              Login
            </Typography>

            <LoginForm />

            <div className={classes.navAuth}>
              Don't have an account?
              <Link className={classes.navLink} to="/registration/">
                Register
              </Link>
            </div>
          </Grid>
        </Paper>
      </div>
    </div>
  )
}

export default withStyles(styles)(Login)
