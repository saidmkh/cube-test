import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'

import { userValidate } from '../../../services/validate'
import { loginDispatch } from '../../../actions/auth'
import { handleStateChange } from '../../../services/helpers'
import history from '../../../services/history'

const styles = theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  submitBtn: {
    marginTop: '2rem',
    width: 200
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  }
})

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      errors: {}
    }

    this.loginSubmit = this.loginSubmit.bind(this)
  }

  loginSubmit = e => {
    e.preventDefault()

    const { email, password } = this.state
    const { loginDispatch } = this.props

    const User = {
      email,
      password
    }

    const { errors, validate } = userValidate(User)

    if (!validate) {
      this.setState({
        errors: errors
      })

      return false
    }

    loginDispatch(User)
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      })
    }
  }

  componentDidMount = () => {
    if (this.props.isLogged) {
      history.push('/')
    }
  }

  render() {
    const { classes } = this.props
    const { email, password, errors } = this.state

    return (
      <form className={classes.form} onSubmit={this.loginSubmit}>
        <TextField
          type="email"
          name="email"
          label="Email"
          value={email}
          onChange={handleStateChange.bind(this)}
          margin="normal"
          required
        />
        {errors.email ? (
          <div className={classes.inputErrors}>{errors.email}</div>
        ) : null}

        <TextField
          type="password"
          name="password"
          label="Password"
          value={password}
          onChange={handleStateChange.bind(this)}
          margin="normal"
          required
        />
        {errors.password ? (
          <div className={classes.inputErrors}>{errors.password}</div>
        ) : null}

        <Button
          className={classes.submitBtn}
          type="submit"
          variant="contained"
          color="primary"
        >
          Login
        </Button>
      </form>
    )
  }
}

const mapStateToProps = store => ({
  isLogged: store.auth.isLogged,
  errors: store.errors.errors
})

const mapDispatchToProps = dispatch => ({
  loginDispatch: user => dispatch(loginDispatch(user))
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(LoginForm))
)
