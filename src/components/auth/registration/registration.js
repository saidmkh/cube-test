import React from 'react'
import { Link } from 'react-router-dom'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

import RegistrationForm from './registration_form'

const styles = theme => ({
  authWrapper: {
    minHeight: '65vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  paper: {
    marginTop: '-64px',
    padding: '1rem'
  },
  navAuth: {
    margin: '1rem 0',
    fontSize: '1rem'
  },
  navLink: {
    color: 'blue',
    fontSize: '1.1rem',
    marginLeft: '3px'
  },
  authBlock: {
    maxWidth: 400,
    width: '100%'
  }
})

const Registration = props => {
  const { classes } = props

  return (
    <div className={classes.authWrapper}>
      <div className={classes.authBlock}>
        <Paper className={classes.paper} spacing={16} p={2}>
          <Grid align="center">
            <Typography align="center" variant="h5" component="h2">
              Register
            </Typography>

            <RegistrationForm />

            <div className={classes.navAuth}>
              <span>Already have an account?</span>
              <Link className={classes.navLink} to="/login/">
                Login
              </Link>
            </div>
          </Grid>
        </Paper>
      </div>
    </div>
  )
}

export default withStyles(styles)(Registration)
