import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'

import { userValidate } from '../../../services/validate'
import { registrationDispatch } from '../../../actions/auth'
import { handleStateChange } from '../../../services/helpers'
import history from '../../../services/history'

const styles = theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  submitBtn: {
    marginTop: '2rem',
    width: 200
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  }
})

class RegistrationForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      password: '',
      errors: {}
    }

    this.submitRegister = this.submitRegister.bind(this)
  }

  submitRegister = e => {
    e.preventDefault()

    const { name, email, password } = this.state
    const { registrationDispatch } = this.props

    const User = {
      name,
      email,
      password
    }

    const { errors, validate } = userValidate(User)
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    registrationDispatch(User)
  }

  componentDidMount = () => {
    if (this.props.isLogged) {
      history.push('/')
    }
  }

  componentWillReceiveProps = nextProps => {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      })
    }
  }

  render() {
    const { classes } = this.props
    const { name, email, password, errors } = this.state

    return (
      <form className={classes.form} onSubmit={this.submitRegister}>
        <TextField
          type="text"
          name="name"
          label="name"
          value={name}
          onChange={handleStateChange.bind(this)}
          margin="normal"
        />
        {errors.name ? (
          <div className={classes.inputErrors}>{errors.name}</div>
        ) : null}

        <TextField
          type="email"
          name="email"
          label="Email"
          value={email}
          onChange={handleStateChange.bind(this)}
          margin="normal"
          required
        />
        {errors.email ? (
          <div className={classes.inputErrors}>{errors.email}</div>
        ) : null}

        <TextField
          type="password"
          name="password"
          label="Password"
          value={password}
          onChange={handleStateChange.bind(this)}
          margin="normal"
          required
        />
        {errors.password ? (
          <div className={classes.inputErrors}>{errors.password}</div>
        ) : null}

        <Button
          className={classes.submitBtn}
          type="submit"
          variant="contained"
          color="primary"
        >
          Register
        </Button>
      </form>
    )
  }
}

const mapStateToProps = store => ({
  isLogged: store.auth.isLogged,
  errors: store.errors.errors
})

const mapDispatchToProps = dispatch => ({
  registrationDispatch: user => dispatch(registrationDispatch(user))
})

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles)(RegistrationForm))
)
