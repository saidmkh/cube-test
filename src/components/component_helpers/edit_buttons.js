import React, { Component } from 'react'
import Typography from '@material-ui/core/Typography'
import { withStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/Delete'
import SaveIcon from '@material-ui/icons/Save'
import Cancel from '@material-ui/icons/Cancel'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  fab: {
    width: 40,
    height: 40,
    margin: theme.spacing.unit
  },
  modal: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },

  button: {
    marginLeft: 20
  }
})

class EditButtons extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      edit: false
    }
  }

  onEditOpen = () => this.setState({ edit: true })

  onEditClose = () => {
    const { stateToProps } = this.props

    stateToProps()
    this.setState({ edit: false })
  }

  openDeleteModal = () => this.setState({ open: true })

  closeDeleteModal = () => this.setState({ open: false })

  handleDeleteClick = () => {
    const { _id, onDelete } = this.props

    onDelete(_id)
    this.setState({ open: false })
  }

  render() {
    const { classes } = this.props
    const { edit } = this.state

    if (edit) {
      return (
        <React.Fragment>
          <Fab aria-label="Save" className={classes.fab} color="primary">
            <SaveIcon />
          </Fab>
          <Fab
            color="secondary"
            aria-label="Cancel"
            className={classes.fab}
            onClick={() => this.onEditClose()}
          >
            <Cancel />
          </Fab>
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment>
          <Fab
            aria-label="Edit"
            className={classes.fab}
            onClick={() => this.onEditOpen()}
          >
            <Icon>edit_icon</Icon>
          </Fab>
          <Fab
            color="secondary"
            aria-label="Delete"
            className={classes.fab}
            onClick={this.openDeleteModal}
          >
            <DeleteIcon />
          </Fab>

          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.closeDeleteModal}
          >
            <div className={classes.modal}>
              <Typography variant="subtitle1" id="simple-modal-description">
                You are sure you want to delete this user?
              </Typography>
              <div className={classes.buttonBlock}>
                <Button
                  variant="contained"
                  className={classes.button}
                  onClick={() => this.closeDeleteModal()}
                >
                  Close
                </Button>
                <Button
                  variant="contained"
                  color="secondary"
                  className={classes.button}
                  onClick={() => this.props.onDelete()}
                >
                  Delete
                </Button>
              </div>
            </div>
          </Modal>
        </React.Fragment>
      )
    }
  }
}

export default withStyles(styles)(EditButtons)
