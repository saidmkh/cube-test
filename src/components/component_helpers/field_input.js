import React from 'react'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  inputWrapper: {
    marginBottom: '30px',
    width: '100%'
  },
  inputLabel: {
    marginBottom: '7px',
    fontSize: '12px',
    marginLeft: '10px'
  },
  inputField: {
    width: '100%',
    fontSize: '20px',
    padding: '9px 10px 8px',
    borderWidth: '0 0 1px 0',
    borderColor: 'black',
    backgroundColor: 'inherit',
    outline: 0,
    '&:disabled': {
      border: 0,
      cursor: 'default',
      color: 'black'
    }
  }
})

const FieldInput = props => {
  const {
    classes,
    title,
    name,
    type,
    value,
    onChange,
    placeholder,
    disabled
  } = props

  return (
    <div className={classes.inputWrapper}>
      <label htmlFor={name} className={classes.inputLabel}>
        {title}:
      </label>
      <input
        className={classes.inputField}
        id={name}
        name={name}
        type={type}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        disabled={disabled}
      />
    </div>
  )
}

export default withStyles(styles)(FieldInput)
