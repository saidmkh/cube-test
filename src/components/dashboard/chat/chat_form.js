import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import TextField from '@material-ui/core/TextField'

const styles = theme => ({
  formBlock: {
    width: '100%',
    display: 'flex'
  },
  sendButton: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  }
})

const ChatForm = props => {
  const { message, sendMessage, onChange, classes } = props

  return (
    <form onSubmit={e => sendMessage(e)} className={classes.formBlock}>
      <TextField
        fullWidth
        name="message"
        type="text"
        value={message}
        onChange={onChange}
        margin="normal"
      />
      <Button
        variant="contained"
        color="primary"
        className={classes.sendButton}
        type="submit"
      >
        Send
        <Icon className={classes.rightIcon}>send</Icon>
      </Button>
    </form>
  )
}

export default withStyles(styles)(ChatForm)
