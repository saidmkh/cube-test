import React from 'react'
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import { apiUrl } from '../../../config/api'

const styles = theme => ({
  avatar: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  },
  userInfoBlock: {
    marginLeft: 20,
    display: 'flex',
    flexDirection: 'column'
  },
  messageText: {
    marginTop: 7,
    wordBreak: 'break-all'
  }
})

const ChatItem = props => {
  const { classes } = props
  const { name, message, avatar, created } = props.message

  return (
    <ListItem>
      <Avatar>
        {avatar ? (
          <img
            className={classes.avatar}
            src={`${apiUrl}uploads/${avatar}`}
            alt=""
          />
        ) : (
          name.charAt(0)
        )}
      </Avatar>
      <div className={classes.userInfoBlock}>
        <ListItemText primary={name} secondary={moment(created).fromNow()} />
        <Typography className={classes.messageText} variant="subtitle1">
          {message}
        </Typography>
      </div>
    </ListItem>
  )
}

export default withStyles(styles)(ChatItem)
