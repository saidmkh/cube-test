import React, { Component } from 'react'
import { connect } from 'react-redux'
import openSocket from 'socket.io-client'
import { withStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import { apiUrl } from '../../../config/api'
import { handleStateChange } from '../../../services/helpers'
import ChatItem from './chat_item'
import ChatForm from './chat_form'
import ChatUser from './chat_user'
import { getMessages } from '../../../actions/chat'
import { chatValidate } from '../../../services/validate'

const styles = theme => ({
  chatWrapper: {
    padding: 20,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: '0 auto',
    height: '100%',
    minHeight: 'calc(100vh - 114px)',
    justifyContent: 'space-between'
  },
  chatContainer: {
    width: '100%',
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column'
    }
  },
  chatUsers: {
    width: 240,
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      order: '1'
    }
  },
  userList: {
    height: '80vh',
    overflow: 'hidden',
    overflowY: 'auto',
    [theme.breakpoints.down('sm')]: {
      height: 130,
      borderBottom: '1px solid'
    }
  },
  chatList: {
    height: '75vh',
    overflow: 'hidden',
    overflowY: 'auto'
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  },
  chatMessagesBlock: {
    flex: '1',
    [theme.breakpoints.down('sm')]: {
      order: '2'
    }
  }
})

export class ChatPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      message: '',
      messages: [],
      users: [],
      errors: {}
    }
  }

  componentDidMount = () => {
    getMessages().then(res => {
      console.log(res)
      const messages = res.data.slice().reverse()

      this.setState({ messages })
    })

    this.connectSockets()

    this.scrollToBottom()
  }

  componentDidUpdate = () => {
    this.scrollToBottom()
  }

  scrollToBottom() {
    this.messageEnd.scrollIntoView({ behavior: 'smooth' })
  }

  connectSockets = () => {
    const { userId } = this.props

    this.socket = openSocket(apiUrl)

    this.socket.on('username-result', res => {
      this.setState({
        users: res
      })
    })

    this.socket.emit('username', userId)

    this.socket.on('new message', data => {
      this.addMessage(data)
    })

    this.socket.on('user left', data => {
      this.setState({
        users: data
      })
    })
  }

  addMessage = data => {
    const { messages } = this.state
    this.setState({ messages: [...messages, data] })
  }

  sendMessage = e => {
    e.preventDefault()
    const { message } = this.state
    const { userId } = this.props
    const Message = { userId, message }

    const { errors, validate } = chatValidate(Message)
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    this.socket.emit('send-from-user', Message, () => {})
    this.setState({ message: '', errors: {} })
  }

  render() {
    const { message, messages, users, errors } = this.state
    const { classes } = this.props

    return (
      <div className={classes.chatWrapper}>
        <div className={classes.chatContainer}>
          <div className={classes.chatMessagesBlock}>
            <List className={classes.chatList}>
              {messages.map((obj, idx) => {
                return <ChatItem key={idx} message={obj} />
              })}
              <div
                ref={element => {
                  this.messageEnd = element
                }}
              />
            </List>
            <ChatForm
              message={message}
              sendMessage={this.sendMessage}
              onChange={handleStateChange.bind(this)}
            />
          </div>
          <div className={classes.chatUsers}>
            <Typography variant="h6" align="center">
              Users connected: {users.length}
            </Typography>
            <div className={classes.userList}>
              {users.map(obj => {
                return <ChatUser key={obj.id} user={obj} />
              })}
            </div>
          </div>
        </div>
        {errors.message ? (
          <div className={classes.inputErrors}>{errors.message}</div>
        ) : null}
      </div>
    )
  }
}

const mapStateToProps = store => ({
  userId: store.auth.user._id
})

export default connect(mapStateToProps)(withStyles(styles)(ChatPage))
