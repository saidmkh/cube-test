import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Avatar from '@material-ui/core/Avatar'
import { apiUrl } from '../../../config/api'

const styles = theme => ({
  avatar: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  }
})

const ChatUser = props => {
  const { classes } = props
  const { name, avatar } = props.user

  return (
    <ListItem>
      <Avatar>
        {avatar ? (
          <img
            className={classes.avatar}
            src={`${apiUrl}uploads/${avatar}`}
            alt=""
          />
        ) : (
          name.charAt(0)
        )}
      </Avatar>
      <ListItemText primary={name} />
    </ListItem>
  )
}

export default withStyles(styles)(ChatUser)
