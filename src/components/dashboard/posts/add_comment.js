import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'

import Icon from '@material-ui/core/Icon'
import Button from '@material-ui/core/Button'
import { handleStateChange } from '../../../services/helpers'
import { commentValidate } from '../../../services/validate'

const styles = theme => ({
  createContainer: {
    width: '100%',
    marginBottom: 30
  },
  uploadBlock: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  inputBlock: {
    marginBottom: 20
  },
  button: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  }
})

class AddComment extends Component {
  constructor(props) {
    super(props)
    this.initialState = {
      content: '',
      errors: {}
    }
    this.state = { ...this.initialState }
  }

  resetState() {
    this.setState({ ...this.initialState })
  }

  handleCreateComment = () => {
    const { content } = this.state
    const { createComment } = this.props

    const { errors, validate } = commentValidate({ content })
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    createComment(content)
    this.resetState()
  }

  render() {
    const { classes } = this.props
    const { content, errors } = this.state

    return (
      <div className={classes.createContainer}>
        <TextField
          className={classes.inputBlock}
          name="content"
          value={content}
          fullWidth
          label="Enter your comment"
          multiline
          rows={4}
          variant="outlined"
          onChange={handleStateChange.bind(this)}
        />
        {errors.content ? (
          <div className={classes.inputErrors}>{errors.content}</div>
        ) : null}
        <div className={classes.uploadBlock}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => this.handleCreateComment()}
          >
            Send
            <Icon className={classes.rightIcon}>send</Icon>
          </Button>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(AddComment)
