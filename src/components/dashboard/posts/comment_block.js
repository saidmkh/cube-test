import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import CommentItem from './comment_item'
import AddComment from './add_comment'
import Typography from '@material-ui/core/Typography'
import {
  createComment,
  updateComment,
  deleteComment
} from '../../../actions/post'

const styles = theme => ({
  commentsBlock: {
    marginTop: 50
  },
  noComments: {
    margin: '2rem 0'
  }
})

class CommentBlock extends Component {
  onDeleteComment = commentId => {
    const { getPost, postId } = this.props

    deleteComment(postId, commentId).then(() => getPost())
  }

  onUpdateComment = (commentId, content) => {
    const { getPost, postId } = this.props

    updateComment(postId, commentId, content).then(() => getPost())
  }

  onCreateComment = content => {
    const { getPost, postId } = this.props

    createComment(postId, content).then(() => getPost())
  }

  render() {
    const { classes, comments } = this.props

    return (
      <div className={classes.commentsBlock}>
        {comments.length ? (
          <React.Fragment>
            {comments.map(obj => {
              return (
                <CommentItem
                  key={obj._id}
                  comment={obj}
                  updateComment={this.onUpdateComment}
                  deleteComment={this.onDeleteComment}
                />
              )
            })}
          </React.Fragment>
        ) : (
          <Typography variant="h6" className={classes.noComments}>
            No comments here
          </Typography>
        )}
        <AddComment createComment={this.onCreateComment} />
      </div>
    )
  }
}

export default withStyles(styles)(CommentBlock)
