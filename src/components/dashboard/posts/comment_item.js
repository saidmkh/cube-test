import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import FavoriteIcon from '@material-ui/icons/Favorite'
import ShareIcon from '@material-ui/icons/Share'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/Delete'
import SaveIcon from '@material-ui/icons/Save'
import Cancel from '@material-ui/icons/Cancel'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { handleStateChange } from '../../../services/helpers'
import { commentValidate } from '../../../services/validate'
import { apiUrl } from '../../../config/api'

const styles = theme => ({
  card: {
    width: '100%',
    marginBottom: 20
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  textField: {
    width: '100%'
  },
  fab: {
    marginRight: 5
  },
  modal: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  buttonBlock: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  button: {
    margin: '30px 0 0 15px'
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  },
  content: {
    wordBreak: 'break-word'
  },
  avatarImg: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  }
})

class CommentItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      open: false,
      content: '',
      errors: {}
    }
  }

  handleDeleteComment = () => {
    const { _id } = this.props.comment
    const { deleteComment } = this.props

    deleteComment(_id)
    this.closeDeleteModal()
  }

  handleUpdateComment = () => {
    const { _id } = this.props.comment
    const { updateComment } = this.props
    const { content } = this.state

    const { errors, validate } = commentValidate({ content })
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    updateComment(_id, content)
    this.setState({ edit: false })
  }

  resetStateToProps = () => {
    const { content } = this.props.comment

    this.setState({
      content
    })
  }

  onEditOpen = () => this.setState({ edit: true })

  openDeleteModal = () => this.setState({ open: true })

  closeDeleteModal = () => this.setState({ open: false })

  onEditClose = () => {
    this.resetStateToProps()
    this.setState({ edit: false })
  }

  componentDidMount = () => {
    this.resetStateToProps()
  }

  handleEditFalse = () => this.setState({ edit: false })

  render() {
    const { classes, user } = this.props
    const { name, imagePath, _id } = this.props.comment.author
    const { edit, content, errors } = this.state

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className={classes.avatar}>
              {imagePath ? (
                <img
                  className={classes.avatarImg}
                  src={`${apiUrl}uploads/${imagePath}`}
                  alt=""
                />
              ) : (
                name.charAt(0)
              )}
            </Avatar>
          }
          title={name}
        />
        <CardContent>
          {edit ? (
            <React.Fragment>
              <TextField
                name="content"
                multiline
                rows={1}
                value={content}
                onChange={handleStateChange.bind(this)}
                className={classes.textField}
                margin="normal"
              />
              {errors.content ? (
                <div className={classes.inputErrors}>{errors.content}</div>
              ) : null}
            </React.Fragment>
          ) : (
            <Typography className={classes.content} component="p">
              {content}
            </Typography>
          )}
        </CardContent>
        <CardActions className={classes.actions} disableActionSpacing>
          <div className={classes.fakeButtons}>
            <IconButton aria-label="Add to favorites">
              <FavoriteIcon />
            </IconButton>
            <IconButton aria-label="Share">
              <ShareIcon />
            </IconButton>
          </div>
          {user.isAdmin || user._id === _id ? (
            <React.Fragment>
              {edit ? (
                <div>
                  <Fab
                    size="small"
                    aria-label="Save"
                    className={classes.fab}
                    color="primary"
                    onClick={() => this.handleUpdateComment()}
                  >
                    <SaveIcon />
                  </Fab>
                  <Fab
                    size="small"
                    color="secondary"
                    aria-label="Cancel"
                    className={classes.fab}
                    onClick={() => this.onEditClose()}
                  >
                    <Cancel />
                  </Fab>
                </div>
              ) : (
                <React.Fragment>
                  <div>
                    <IconButton
                      aria-label="Delete"
                      className={classes.fab}
                      onClick={() => this.onEditOpen()}
                      color="primary"
                    >
                      <Icon>edit_icon</Icon>
                    </IconButton>
                    <IconButton
                      aria-label="Delete"
                      className={classes.fab}
                      onClick={this.openDeleteModal}
                      color="secondary"
                    >
                      <DeleteIcon />
                    </IconButton>
                  </div>
                  <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.open}
                    name="open"
                    onClose={this.closeDeleteModal}
                  >
                    <div className={classes.modal}>
                      <Typography
                        variant="subtitle1"
                        id="simple-modal-description"
                      >
                        You are sure you want to delete this comment?
                      </Typography>
                      <div className={classes.buttonBlock}>
                        <Button
                          variant="contained"
                          name="open"
                          className={classes.button}
                          onClick={this.closeDeleteModal}
                        >
                          Close
                        </Button>
                        <Button
                          variant="contained"
                          color="secondary"
                          className={classes.button}
                          onClick={() => this.handleDeleteComment()}
                        >
                          Delete
                        </Button>
                      </div>
                    </div>
                  </Modal>
                </React.Fragment>
              )}
            </React.Fragment>
          ) : null}
        </CardActions>
      </Card>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
})

export default connect(mapStateToProps)(withStyles(styles)(CommentItem))
