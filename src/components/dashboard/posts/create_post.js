import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Fab from '@material-ui/core/Fab'
import SaveIcon from '@material-ui/icons/Save'
import { handleStateChange } from '../../../services/helpers'
import { createPost } from '../../../actions/post'
import { postValidate } from '../../../services/validate'

const styles = theme => ({
  createContainer: {
    width: '100%',
    marginBottom: 30
  },
  uploadBlock: {
    display: 'flex',
    alignItems: 'center'
  },
  inputBlock: {
    marginBottom: 20
  },
  saveBtn: {
    marginLeft: 'auto'
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem .5rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    marginBottom: 7
  }
})

class CreatePost extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      description: '',
      image: null,
      errors: {}
    }
  }

  handleSelectedImage = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0]
      this.setState({ image })
    }
  }

  onCreatePost = () => {
    const { onCreated } = this.props
    const { title, description, image } = this.state

    const { errors, validate } = postValidate({ title, description })
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }
    const fData = new FormData()

    fData.append('imageFile', image)
    fData.append('title', title)
    fData.append('description', description)

    createPost(fData).then(() => onCreated())
  }

  render() {
    const { classes } = this.props
    const { title, description, errors } = this.state

    return (
      <div className={classes.createContainer}>
        <TextField
          className={classes.inputBlock}
          name="title"
          value={title}
          fullWidth
          label="title"
          variant="outlined"
          onChange={handleStateChange.bind(this)}
        />
        {errors.title ? (
          <div className={classes.inputErrors}>{errors.title}</div>
        ) : null}
        <TextField
          className={classes.inputBlock}
          name="description"
          value={description}
          fullWidth
          label="description"
          multiline
          rows={4}
          variant="outlined"
          onChange={handleStateChange.bind(this)}
        />
        {errors.description ? (
          <div className={classes.inputErrors}>{errors.description}</div>
        ) : null}
        <div className={classes.uploadBlock}>
          <input
            className={classes.fileInput}
            type="file"
            accept="image/*"
            name="image"
            onChange={this.handleSelectedImage}
          />
          <Fab
            aria-label="Save"
            className={classes.saveBtn}
            color="secondary"
            onClick={() => this.onCreatePost()}
          >
            <SaveIcon />
          </Fab>
        </div>
      </div>
    )
  }
}

export default withStyles(styles)(CreatePost)
