import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import PostItem from './post_map_item'
import CreatePost from './create_post'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import Cancel from '@material-ui/icons/Cancel'
import { getAllPosts } from '../../../actions/post'

const styles = theme => ({
  postListWrapper: {
    padding: 25,
    maxWidth: '1200px',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    margin: '0 auto'
  },
  listTitle: {
    textAlign: 'center',
    marginBottom: '1.5rem'
  },
  createPostBlock: {
    width: '100%'
  },
  cancelBtnBlock: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: 30
  }
})

export class PostMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      posts: [],
      createPost: false
    }
  }

  openCreatePost = () => this.setState({ createPost: true })

  closeCreatePost = () => this.setState({ createPost: false })

  getPosts = () => {
    getAllPosts().then(res => {
      const posts = res.data.slice().reverse()
      this.setState({
        posts
      })
    })
  }

  componentDidMount = () => {
    this.getPosts()
  }

  onCreated = () => {
    this.closeCreatePost()
    this.getPosts()
  }

  render() {
    const { classes } = this.props
    const { posts, createPost } = this.state

    return (
      <div className={classes.postListWrapper}>
        <Typography className={classes.listTitle} variant="h4">
          Posts
        </Typography>
        {createPost ? (
          <div className={classes.createPostBlock}>
            <div className={classes.cancelBtnBlock}>
              <Fab
                color="primary"
                aria-label="Cancel"
                onClick={() => this.closeCreatePost()}
              >
                <Cancel />
              </Fab>
            </div>
            <CreatePost onCreated={() => this.onCreated()} />
          </div>
        ) : (
          <div className={classes.cancelBtnBlock}>
            <Fab
              color="primary"
              aria-label="Add"
              onClick={() => this.openCreatePost()}
            >
              <AddIcon />
            </Fab>
          </div>
        )}

        {posts.map((obj, idx) => {
          return <PostItem key={idx} obj={obj} />
        })}
      </div>
    )
  }
}

export default withStyles(styles)(PostMain)
