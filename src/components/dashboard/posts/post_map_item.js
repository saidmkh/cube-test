import React from 'react'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import { apiUrl } from '../../../config/api'

const styles = {
  card: {
    width: '100%',
    marginBottom: 30
  },
  media: {
    height: 440
  },
  description: {
    maxHeight: '60px',
    overflow: 'hidden'
  }
}

const PostItem = props => {
  const { title, imageFile, description, created_user, _id } = props.obj
  const { classes } = props

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          gutterBottom
          variant="subtitle1"
          className={classes.postText}
        >
          {created_user.name}
        </Typography>
      </CardContent>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {title}
        </Typography>
      </CardContent>
      {imageFile ? (
        <CardMedia
          className={classes.media}
          image={`${apiUrl}uploads/${imageFile}`}
          title="Contemplative Reptile"
        />
      ) : null}
      <CardContent className={classes.description}>
        <Typography component="p">{description}</Typography>
      </CardContent>
      <CardActions>
        <Link to={`/post/${_id}`}>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </Link>
      </CardActions>
    </Card>
  )
}

export default withStyles(styles)(PostItem)
