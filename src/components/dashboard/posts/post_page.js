import React, { Component } from 'react'
import { connect } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/Delete'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import { apiUrl } from '../../../config/api'
import { handleStateChange } from '../../../services/helpers'
import CommentBlock from './comment_block'
import history from '../../../services/history'
import { postValidate } from '../../../services/validate'
import { updatePost, deletePost, getPost } from '../../../actions/post'

const styles = theme => ({
  postListWrapper: {
    maxWidth: '1200px',
    width: '100%',
    margin: '0 auto',
    padding: 25
  },
  listTitle: {
    marginTop: '3rem',
    textAlign: 'center',
    marginBottom: '1.5rem'
  },
  imgWrap: {
    width: '100%',
    height: 600
  },
  postImg: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center'
  },
  fab: {
    width: 40,
    height: 40,
    margin: theme.spacing.unit
  },
  modal: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  buttonBlock: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  button: {
    marginLeft: 20
  },
  modalButton: {
    margin: '30px 0 0 15px'
  },
  commentsBlock: {
    marginTop: 50
  },
  uploadBlock: {
    margin: '10px 0',
    display: 'flex',
    width: 220,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  fileInput: {
    width: 150
  },
  postDescription: {
    marginTop: 50
  },
  modalInputBlock: {
    width: '100%',
    marginBottom: 30
  },
  editInput: {
    width: '100%'
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  }
})

class PostPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _id: '',
      title: '',
      imageFile: '',
      description: '',
      created_user: {},
      comments: [],
      image: null,
      previewImg: '',
      edit: false,
      open: false,
      errors: {}
    }
  }

  onEditOpen = () => this.setState({ edit: true })

  openDeleteModal = () => this.setState({ open: true })

  closeDeleteModal = () => this.setState({ open: false })

  onEditClose = () => {
    this.setPostToState()
    this.setState({
      edit: false,
      errors: {}
    })
  }

  componentDidMount = () => {
    window.scrollTo(0, 0)

    this.setPostToState()
  }

  setPostToState = () => {
    const postId = this.props.match.params.id

    getPost(postId).then(res => {
      console.log(res)
      this.setState({
        _id: res.data._id,
        title: res.data.title,
        imageFile: res.data.imageFile,
        description: res.data.description,
        created_user: res.data.created_user,
        comments: res.data.comments
      })
    })
  }

  handleDeletePost = () => {
    const { _id } = this.state

    deletePost(_id).then(res => {
      console.log(res)
      history.push('/')
    })
  }

  handleSelectedImage = e => {
    if (e.target.files[0]) {
      const reader = new FileReader()
      const image = e.target.files[0]

      reader.onloadend = () => {
        this.setState({
          image,
          previewImg: reader.result
        })
      }

      reader.readAsDataURL(image)
    }
  }

  updatePost = () => {
    const { _id, title, description, image } = this.state

    const { errors, validate } = postValidate({ title, description })
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    const fData = new FormData()

    if (image) {
      fData.append('imageFile', image)
    }

    fData.append('title', title)
    fData.append('description', description)

    updatePost(_id, fData).then(res => {
      this.onEditClose()
      this.setPostToState()
    })
  }

  render() {
    const { classes, user } = this.props
    const {
      _id,
      title,
      imageFile,
      description,
      created_user,
      comments,
      edit,
      errors,
      imagePreviewUrl
    } = this.state

    return (
      <div className={classes.postListWrapper}>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={edit}
          onClose={this.onEditClose}
        >
          <div className={classes.modal}>
            <Typography variant="h6" id="modal-title">
              Edit post
            </Typography>
            <div className={classes.modalInputBlock}>
              <TextField
                className={classes.editInput}
                name="title"
                value={title}
                onChange={handleStateChange.bind(this)}
                margin="normal"
              />
              {errors.title ? (
                <div className={classes.inputErrors}>{errors.title}</div>
              ) : null}
            </div>
            <div className={classes.modalInputBlock}>
              <TextField
                className={classes.editInput}
                name="description"
                multiline
                rows={5}
                value={description}
                onChange={handleStateChange.bind(this)}
                margin="normal"
              />
              {errors.description ? (
                <div className={classes.inputErrors}>{errors.description}</div>
              ) : null}
            </div>
            <div className={classes.uploadBlock}>
              <input
                className={classes.fileInput}
                type="file"
                accept="image/*"
                name="image"
                onChange={this.handleSelectedImage}
              />
            </div>
            <div className={classes.buttonBlock}>
              <Button
                variant="contained"
                className={classes.modalButton}
                onClick={() => this.onEditClose()}
              >
                Close
              </Button>
              <Button
                variant="contained"
                color="secondary"
                className={classes.modalButton}
                onClick={() => this.updatePost()}
              >
                Save
              </Button>
            </div>
          </div>
        </Modal>
        <Typography className={classes.listTitle} variant="h4">
          {title}
        </Typography>
        <div className={classes.buttonBlock}>
          <Typography variant="subtitle2">{created_user.name}</Typography>

          {user.isAdmin || user._id === created_user._id ? (
            <div>
              <Fab
                aria-label="Edit"
                className={classes.fab}
                onClick={() => this.onEditOpen()}
              >
                <Icon>edit_icon</Icon>
              </Fab>
              <Fab
                color="secondary"
                aria-label="Delete"
                className={classes.fab}
                onClick={() => this.openDeleteModal()}
              >
                <DeleteIcon />
              </Fab>

              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={() => this.closeDeleteModal()}
              >
                <div className={classes.modal}>
                  <Typography variant="subtitle1" id="simple-modal-description">
                    You are sure you want to delete this post?
                  </Typography>
                  <div className={classes.buttonBlock}>
                    <Button
                      variant="contained"
                      className={classes.modalButton}
                      onClick={() => this.closeDeleteModal()}
                    >
                      Close
                    </Button>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.modalButton}
                      onClick={() => this.handleDeletePost()}
                    >
                      Delete
                    </Button>
                  </div>
                </div>
              </Modal>
            </div>
          ) : null}
        </div>
        {imageFile ? (
          <div className={classes.imgWrap}>
            <img
              src={`${apiUrl}uploads/${imageFile}`}
              alt=""
              className={classes.postImg}
            />
          </div>
        ) : null}
        <Typography variant="subtitle1" className={classes.postDescription}>
          {description}
        </Typography>
        <CommentBlock
          comments={comments}
          getPost={this.setPostToState}
          postId={_id}
        />
      </div>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
})

export default connect(mapStateToProps)(withStyles(styles)(PostPage))
