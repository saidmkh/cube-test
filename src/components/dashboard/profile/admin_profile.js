import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import ProfileItem from './profile_item'
import Typography from '@material-ui/core/Typography'
import { deleteUser, updateUser, getUsers } from '../../../actions/user'

const styles = theme => ({
  profileListWrapper: {
    maxWidth: '1200px',
    width: '100%',
    margin: 'auto'
  },
  listTitle: {
    textAlign: 'center',
    marginBottom: '1.5rem'
  }
})

class AdminProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      users: []
    }
  }

  setUsersToState = () => {
    getUsers().then(res => this.setState({ users: res.data.users }))
  }

  componentDidMount = () => {
    this.setUsersToState()
  }

  handleDeleteUser = userId => {
    deleteUser(userId).then(res => {
      console.log(res)
      this.setUsersToState()
    })
  }

  render() {
    const { classes, isAdmin } = this.props
    const { users } = this.state

    return (
      <div className={classes.profileListWrapper}>
        <Typography className={classes.listTitle} variant="h4">
          Users List
        </Typography>
        {users.map(obj => {
          return (
            <ProfileItem
              key={obj._id}
              profile={obj}
              lg_size={8}
              sm_size={9}
              authUserAdmin={isAdmin}
              deleteUser={this.handleDeleteUser}
            />
          )
        })}
      </div>
    )
  }
}

const mapStateToProps = store => ({
  isAdmin: store.auth.user.isAdmin
})

export default connect(mapStateToProps)(withStyles(styles)(AdminProfile))
