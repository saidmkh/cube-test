import React, { Component } from 'react'
import { connect } from 'react-redux'
import UserProfile from './user_profile'
import AdminProfile from './admin_profile'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  profileWrapper: {
    flex: 1,
    padding: 25
  }
})

export class ProfilePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      user: {}
    }
  }

  componentDidMount = () => {
    this.changeStateToProps()
  }

  changeStateToProps = () => {
    const { user } = this.props

    this.setState({
      user
    })
  }

  render() {
    const { classes } = this.props
    const { isAdmin } = this.props.user
    const { user } = this.state

    return (
      <div className={classes.profileWrapper}>
        {isAdmin ? (
          <AdminProfile />
        ) : (
          <UserProfile lg_size={8} sm_size={10} obj={user} />
        )}
      </div>
    )
  }
}

const mapStateToProps = store => ({
  isLogged: store.auth.isLogged,
  user: store.auth.user
})

export default connect(mapStateToProps)(withStyles(styles)(ProfilePage))
