import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'
import { handleStateChange } from '../../../services/helpers'
import HowToReg from '@material-ui/icons/HowToReg'
import Email from '@material-ui/icons/Email'
import Face from '@material-ui/icons/Face'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/Delete'
import Typography from '@material-ui/core/Typography'
import Modal from '@material-ui/core/Modal'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import { updateUser } from '../../../actions/user'

import { apiUrl } from '../../../config/api'

const styles = theme => ({
  profileWrap: {
    padding: '1rem 0',
    borderBottom: '1px solid silver'
  },
  imageWrap: {
    height: '150px',
    width: '100%'
  },
  profileImg: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center'
  },
  adminIcon: {
    marginLeft: 15,
    position: 'absolute'
  },
  fab: {
    width: 40,
    height: 40,
    margin: theme.spacing.unit
  },
  modal: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  buttonBlock: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 15
  },
  button: {
    marginLeft: 20
  },
  uploadBlock: {
    display: 'flex',
    width: 220,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  fileInput: {
    width: 150
  },
  flexEnd: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  buttonUpload: {
    margin: theme.spacing.unit
  },
  rightIcon: {
    marginLeft: theme.spacing.unit
  },
  modalInputBlock: {
    width: '100%',
    margin: '20px 0'
  },
  flexContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  infoIcons: {
    marginRight: 7
  }
})

class ProfileItem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      open: false,
      name: '',
      email: '',
      isAdmin: false,
      imagePath: '',
      id: '',
      image: null,
      progress: 0
    }

    this.handleSelectedImage = this.handleSelectedImage.bind(this)
  }

  onEditOpen = () => this.setState({ edit: true })

  openDeleteModal = () => this.setState({ open: true })

  closeDeleteModal = () => this.setState({ open: false })

  onEditClose = () => {
    this.changeStateToProps()
    this.setState({ edit: false })
  }

  changeStateToProps = () => {
    const { name, email, isAdmin, imagePath, _id } = this.props.profile

    this.setState({
      name,
      email,
      isAdmin,
      imagePath,
      _id
    })
  }

  handleAdminCheck = name => e => {
    this.setState({ [name]: e.target.checked })
  }

  componentDidMount = () => {
    this.changeStateToProps()
  }

  handleSelectedImage = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0]
      this.setState({ image })
    }
  }

  handleUpdateUser = () => {
    const { _id, image, name, isAdmin } = this.state

    const fData = new FormData()

    if (image) {
      fData.append('imageFile', image)
    }

    fData.append('name', name)
    fData.append('isAdmin', isAdmin)

    updateUser(_id, fData).then(res => {
      this.setState({
        imagePath: res.data[1].imagePath,
        edit: false
      })
    })
  }

  handleDeleteUser = () => {
    const { deleteUser } = this.props
    const { _id } = this.props.profile

    deleteUser(_id)
    this.setState({ open: false })
  }

  render() {
    const { email, name, isAdmin, imagePath, edit } = this.state
    const { classes, lg_size, sm_size } = this.props

    return (
      <div className={classes.profileWrap}>
        <Grid container justify="flex-start">
          {isAdmin ? (
            <HowToReg className={classes.adminIcon} color="primary" />
          ) : null}
          <Grid
            spacing={24}
            container
            item
            lg={lg_size}
            sm={sm_size}
            xs={12}
            justify="center"
          >
            <Grid item lg={3} sm={4} xs={12}>
              <div className={classes.imageWrap}>
                <img
                  src={
                    imagePath
                      ? `${apiUrl}uploads/${imagePath}`
                      : 'https://ahmetmurati.com/wp-content/themes/verity/assets/images/no-thumb.jpg'
                  }
                  alt=""
                  className={classes.profileImg}
                />
              </div>
            </Grid>
            <Grid item lg={4} sm={5} xs={12}>
              <Typography variant="overline">name:</Typography>
              <div className={classes.flexContainer}>
                <Face className={classes.infoIcons} color="action" />
                <Typography variant="subtitle1">{name}</Typography>
              </div>
              <Typography variant="overline">email:</Typography>
              <div className={classes.flexContainer}>
                <Email className={classes.infoIcons} color="action" />
                <Typography variant="subtitle1">{email}</Typography>
              </div>
            </Grid>
            <Grid className={classes.flexEnd} item xs={3}>
              <div>
                <Fab
                  aria-label="Edit"
                  name="edit"
                  className={classes.fab}
                  onClick={() => this.onEditOpen()}
                >
                  <Icon>edit_icon</Icon>
                </Fab>
                <Fab
                  color="secondary"
                  name="open"
                  aria-label="Delete"
                  className={classes.fab}
                  onClick={this.openDeleteModal}
                >
                  <DeleteIcon />
                </Fab>
              </div>
              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={edit}
                onClose={this.onEditClose}
              >
                <div className={classes.modal}>
                  <Typography variant="h6" id="modal-title">
                    Edit profile
                  </Typography>
                  <div className={classes.imageWrap}>
                    <img
                      src={
                        imagePath
                          ? `${apiUrl}uploads/${imagePath}`
                          : 'https://ahmetmurati.com/wp-content/themes/verity/assets/images/no-thumb.jpg'
                      }
                      alt=""
                      className={classes.profileImg}
                    />
                    <div className={classes.uploadBlock}>
                      <input
                        className={classes.fileInput}
                        type="file"
                        accept="image/*"
                        name="image"
                        onChange={this.handleSelectedImage}
                      />
                    </div>
                  </div>
                  <div className={classes.modalInputBlock}>
                    <TextField
                      fullWidth
                      className={classes.editInput}
                      name="name"
                      value={name}
                      label="name"
                      onChange={handleStateChange.bind(this)}
                      margin="normal"
                    />
                  </div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state.isAdmin}
                        onChange={this.handleAdminCheck('isAdmin')}
                        value="isAdmin"
                        color="primary"
                      />
                    }
                    label="Admin"
                  />

                  <div className={classes.buttonBlock}>
                    <Button
                      variant="contained"
                      className={classes.button}
                      onClick={() => this.onEditClose()}
                    >
                      Close
                    </Button>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      onClick={() => this.handleUpdateUser()}
                    >
                      Save
                    </Button>
                  </div>
                </div>
              </Modal>
              <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                name="open"
                onClose={this.closeDeleteModal}
              >
                <div className={classes.modal}>
                  <Typography variant="subtitle1" id="simple-modal-description">
                    You are sure you want to delete this user?
                  </Typography>
                  <div className={classes.buttonBlock}>
                    <Button
                      variant="contained"
                      name="open"
                      className={classes.button}
                      onClick={this.closeDeleteModal}
                    >
                      Close
                    </Button>
                    <Button
                      variant="contained"
                      color="secondary"
                      className={classes.button}
                      onClick={() => this.handleDeleteUser()}
                    >
                      Delete
                    </Button>
                  </div>
                </div>
              </Modal>
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default withStyles(styles)(ProfileItem)
