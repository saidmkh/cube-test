import React, { Component } from 'react'
import { connect } from 'react-redux'
import jwt_decode from 'jwt-decode'
import setToken from '../../../config/set_token'
import store from '../../../store/store'
import { setCurrentUser } from '../../../actions/auth'
import Grid from '@material-ui/core/Grid'
import FieldInput from '../../component_helpers/field_input'
import { withStyles } from '@material-ui/core/styles'
import { handleStateChange } from '../../../services/helpers'
import Fab from '@material-ui/core/Fab'
import Icon from '@material-ui/core/Icon'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'
import Cancel from '@material-ui/icons/Cancel'
import { updateUser } from '../../../actions/user'
import { userValidate } from '../../../services/validate'
import { loginDispatch, logoutDispatch } from '../../../actions/auth'
import Cropper from 'react-cropper'
import 'cropperjs/dist/cropper.css'
import { apiUrl } from '../../../config/api'
import { Typography } from '@material-ui/core'

const styles = theme => ({
  profileWrap: {
    padding: '1rem 0',
    borderBottom: '1px solid silver'
  },
  imageWrap: {
    height: '150px',
    width: '100%'
  },
  profileImg: {
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    objectPosition: 'center'
  },
  adminIcon: {
    marginLeft: 15,
    position: 'absolute'
  },
  fab: {
    width: 40,
    height: 40,
    margin: theme.spacing.unit
  },
  modal: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
    outline: 'none',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)'
  },
  buttonBlock: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: 15
  },
  button: {
    marginLeft: 20
  },
  uploadBlock: {
    display: 'flex',
    width: 220,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  fileInput: {
    width: 150
  },
  inputErrors: {
    width: 'fit-content',
    padding: '.1rem 1rem',
    fontSize: '.9rem',
    color: 'red',
    borderRadius: '3px',
    margin: 0
  },
  croppedImg: {
    width: 300,
    height: 240
  },
  croppedImgBlock: {
    marginTop: 20,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  cropBlock: {
    marginTop: 30
  },
  cropBlock: {
    marginTop: 30,
    display: 'flex',
    flexDirection: 'column'
  }
})

class UserProfile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      edit: false,
      open: false,
      name: '',
      email: '',
      imagePath: '',
      id: '',
      progress: 0,
      cropUrl: '',
      previewUrl: '',
      cropImg: null,
      errors: {}
    }
  }

  handleSelectedImage = e => {
    if (e.target.files[0]) {
      const image = e.target.files[0]
      const reader = new FileReader()

      reader.onload = () => {
        this.setState({
          cropUrl: reader.result,
          previewUrl: reader.result,
          image
        })
      }

      reader.readAsDataURL(e.target.files[0])
    }
  }

  _crop = () => {
    const cropUrl = this.refs.cropper.getCroppedCanvas().toDataURL()
    console.log('crop')
    return cropUrl
  }

  handleCropImage = () => {
    let cropUrl = this._crop()

    this.refs.cropper.getCroppedCanvas().toBlob(blob => {
      if (!blob) {
        return
      }
      this.setState({
        cropImg: blob,
        cropUrl
      })
    })
  }

  changeStateToProps = () => {
    const { name, email, imagePath, _id } = this.props.user

    this.setState({
      name,
      email,
      imagePath,
      _id
    })
  }

  componentDidMount = () => {
    this.changeStateToProps()
  }

  onEditOpen = () => this.setState({ edit: true })

  onEditClose = () => {
    this.changeStateToProps()
    this.setState({ edit: false })
  }

  openDeleteModal = () => this.setState({ open: true })

  closeDeleteModal = () => this.setState({ open: false })

  handleUpdateUser = () => {
    const { name, email, _id, cropImg, image } = this.state

    const { errors, validate } = userValidate({ name, email })
    if (!validate) {
      this.setState({
        errors: errors
      })
      return false
    }

    const fData = new FormData()

    if (cropImg) {
      fData.append('imageFile', cropImg)
    } else if (image) {
      fData.append('imageFile', image)
    }

    fData.append('name', name)
    fData.append('email', email)

    updateUser(_id, fData).then(res => {
      const token = res.data[2]
      localStorage.setItem('jwtToken', `Bearer ${token}`)
      setToken(localStorage.jwtToken)
      const decoded = jwt_decode(localStorage.jwtToken)
      store.dispatch(setCurrentUser(decoded))

      this.setState({ edit: false, errors: {} })
      this.changeStateToProps()
    })
  }

  render() {
    const {
      email,
      name,
      imagePath,
      errors,
      cropUrl,
      cropImg,
      previewUrl
    } = this.state
    const { classes, lg_size, sm_size } = this.props
    const { edit } = this.state
    console.log(imagePath)

    return (
      <div className={classes.profileWrap}>
        <Grid container justify="flex-start">
          <Grid
            spacing={24}
            container
            item
            lg={lg_size}
            sm={sm_size}
            xs={12}
            justify="center"
          >
            <Grid item lg={3} sm={4} xs={12}>
              <div className={classes.imageWrap}>
                <img
                  src={
                    imagePath
                      ? `${apiUrl}uploads/${imagePath}`
                      : 'https://ahmetmurati.com/wp-content/themes/verity/assets/images/no-thumb.jpg'
                  }
                  alt=""
                  className={classes.profileImg}
                />
                {edit ? (
                  <div className={classes.uploadBlock}>
                    <input
                      className={classes.fileInput}
                      type="file"
                      accept="image/*"
                      name="image"
                      onChange={e => this.handleSelectedImage(e)}
                    />
                  </div>
                ) : null}
              </div>
            </Grid>
            <Grid item lg={4} sm={5} xs={12}>
              <FieldInput
                title="name"
                name="name"
                value={name}
                onChange={handleStateChange.bind(this)}
                disabled={edit ? false : true}
              />
              {errors.name ? (
                <div className={classes.inputErrors}>{errors.name}</div>
              ) : null}
              <FieldInput
                type="email"
                title="email"
                name="email"
                value={email}
                onChange={handleStateChange.bind(this)}
                disabled={edit ? false : true}
              />
              {errors.email ? (
                <div className={classes.inputErrors}>{errors.email}</div>
              ) : null}
            </Grid>
            <Grid item xs={3}>
              {edit ? (
                <React.Fragment>
                  <Fab
                    aria-label="Save"
                    className={classes.fab}
                    color="primary"
                    onClick={() => this.handleUpdateUser()}
                  >
                    <SaveIcon />
                  </Fab>
                  <Fab
                    color="secondary"
                    aria-label="Cancel"
                    className={classes.fab}
                    onClick={() => this.onEditClose()}
                  >
                    <Cancel />
                  </Fab>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  <Fab
                    aria-label="Edit"
                    className={classes.fab}
                    onClick={() => this.onEditOpen()}
                  >
                    <Icon>edit_icon</Icon>
                  </Fab>
                </React.Fragment>
              )}
            </Grid>
          </Grid>
          {edit ? (
            <React.Fragment>
              <Grid
                item
                lg={6}
                md={6}
                sm={12}
                xs={12}
                className={classes.cropBlock}
              >
                <Cropper
                  ref="cropper"
                  src={
                    previewUrl ? previewUrl : `${apiUrl}uploads/${imagePath}`
                  }
                  style={{ height: 300, width: '100%' }}
                  aspectRatio={4 / 3}
                  guides={false}
                  crop={this._crop.bind(this)}
                />
                <Button
                  variant="contained"
                  size="large"
                  color="secondary"
                  className={classes.margin}
                  onClick={() => this.handleCropImage()}
                >
                  Crop
                </Button>
              </Grid>
              {cropUrl ? (
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={12}
                  xs={12}
                  className={classes.croppedImgBlock}
                >
                  <Typography variant="h5">Preview image</Typography>
                  <img
                    className={classes.croppedImg}
                    src={this.state.cropUrl}
                    alt=""
                  />
                </Grid>
              ) : null}
            </React.Fragment>
          ) : null}
        </Grid>
      </div>
    )
  }
}

const mapStateToProps = store => ({
  user: store.auth.user
})

export default connect(
  mapStateToProps,
  { loginDispatch, logoutDispatch, setCurrentUser }
)(withStyles(styles)(UserProfile))
