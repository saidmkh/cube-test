import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import MenuIcon from '@material-ui/icons/Menu'
import AccountCircle from '@material-ui/icons/AccountCircle'
import { logoutDispatch } from '../../actions/auth'
import { menuItems, profileItems } from './menu_items'
import MenuButton from './menu_button'

const styles = {
  userName: {
    marginLeft: 'auto',
    marginRight: 10
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
}

const Header = props => {
  const { classes, userName, logoutDispatch } = props

  return (
    <AppBar position="fixed">
      <Toolbar>
        <MenuButton iconType={MenuIcon} items={menuItems} />

        <Typography
          variant="subtitle1"
          color="inherit"
          className={classes.userName}
        >
          {userName}
        </Typography>

        <MenuButton
          iconType={AccountCircle}
          items={profileItems}
          logout={logoutDispatch}
        />
      </Toolbar>
    </AppBar>
  )
}

const mapStateToProps = store => ({
  userName: store.auth.user.name
})

const mapDispatchToProps = dispatch => ({
  logoutDispatch: () => dispatch(logoutDispatch())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Header))
