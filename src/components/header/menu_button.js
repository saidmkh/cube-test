import React from 'react'
import { Link } from 'react-router-dom'
import IconButton from '@material-ui/core/IconButton'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'

class MenuButton extends React.Component {
  state = {
    anchorEl: null
  }

  handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  render() {
    const { items, logout } = this.props
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)
    const Wrapper = this.props.iconType

    return (
      <div>
        <IconButton
          aria-owns={open ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this.handleMenu}
          color="inherit"
        >
          {<Wrapper />}
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={open}
          onClose={this.handleClose}
        >
          {items.map((item, idx) => {
            return (
              <Link key={idx} to={item.link}>
                <MenuItem onClick={this.handleClose}>{item.title}</MenuItem>
              </Link>
            )
          })}
          {logout ? <MenuItem onClick={() => logout()}>Logout</MenuItem> : null}
        </Menu>
      </div>
    )
  }
}

export default MenuButton
