export const menuItems = [
  {
    link: '/',
    title: 'Posts'
  },
  {
    link: '/about-us/',
    title: 'About Us'
  },
  {
    link: '/chat/',
    title: 'Chat'
  }
]

export const profileItems = [
  {
    link: '/profile/',
    title: 'Profile'
  }
]
