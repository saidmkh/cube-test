import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import PrivateRoute from '../../services/private_route'

import Header from '../header/header'
import PostMain from '../dashboard/posts/post_main'
import Registration from '../auth/registration/registration'
import Login from '../auth/login/login'
import ProfilePage from '../dashboard/profile/profile'
import PostPage from '../dashboard/posts/post_page'
import AboutPage from '../dashboard/about_us/about_us'
import ChatPage from '../dashboard/chat/chat_page'

const styles = theme => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    paddingTop: '64px',
    minHeight: 'calc(100vh - 64px)'
  }
})

class Main extends Component {
  render() {
    const { classes, isLogged } = this.props

    return (
      <div className={classes.wrapper}>
        {isLogged ? <Route component={Header} /> : null}
        <Switch>
          <Route path="/login/" component={Login} />
          <Route path="/registration/" component={Registration} />

          <PrivateRoute exact path="/" component={PostMain} Auth={isLogged} />
          <PrivateRoute
            exact
            path="/post/:id/"
            component={PostPage}
            Auth={isLogged}
          />
          <PrivateRoute
            exact
            path="/profile/"
            component={ProfilePage}
            Auth={isLogged}
          />
          <PrivateRoute
            exact
            path="/about-us/"
            component={AboutPage}
            Auth={isLogged}
          />
          <PrivateRoute
            exact
            path="/chat/"
            component={ChatPage}
            Auth={isLogged}
          />

          <Route render={() => <Redirect to={{ pathname: '/' }} />} />
        </Switch>
      </div>
    )
  }
}

const mapStateToProps = store => ({
  isLogged: store.auth.isLogged
})

export default withRouter(connect(mapStateToProps)(withStyles(styles)(Main)))
