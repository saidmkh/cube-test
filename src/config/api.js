import axios from 'axios'

export const apiUrl = `http://192.168.88.193:4000/`

export default axios.create({
  baseURL: apiUrl
})
