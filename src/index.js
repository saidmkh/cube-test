import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import jwt_decode from 'jwt-decode'
import './styles/normalize.css'
import './styles/index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import store from './store/store'
import history from './services/history'
import setToken from './config/set_token'
import { setCurrentUser, logoutDispatch } from './actions/auth'

if (localStorage.jwtToken) {
  setToken(localStorage.jwtToken)
  const decoded = jwt_decode(localStorage.jwtToken)
  store.dispatch(setCurrentUser(decoded))

  const currentTime = Date.now() / 1000
  if (decoded.exp < currentTime) {
    store.dispatch(logoutDispatch())
  }
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
