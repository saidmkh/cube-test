import { SET_CURRENT_USER } from '../actions/types'
import isEmpty from '../services/isEmpty'

const initialState = {
  isLogged: false,
  user: {}
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isLogged: !isEmpty(action.payload),
        user: action.payload.user
      }
    default:
      return state
  }
}

export default authReducer
