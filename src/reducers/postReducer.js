import { GET_POST } from '../actions/types'

const initialState = {
  post: {}
}

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POST:
      return {
        ...state,
        post: action.payload
      }
    default:
      return state
  }
}

export default postReducer
