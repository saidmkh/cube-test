import { combineReducers } from 'redux'
import authReducer from '../reducers/authReducer'
import userReducer from '../reducers/userReducer'
import postReducer from '../reducers/postReducer'
import errorReducer from '../reducers/errorReducer'

export const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  post: postReducer,
  errors: errorReducer
})
