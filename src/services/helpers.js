import API from '../config/api'

export function handleStateChange(e) {
  return this.setState({ [e.target.name]: e.target.value })
}

export const uploadImage = (url, image) => {
  const imageFile = new FormData()

  imageFile.append('imageFile', image)

  const config = {
    headers: {
      'content-type': 'multipart/form-data'
    }
  }

  API.put(url, imageFile, config)
}

export function toggleTrue(e) {
  this.setState({ [e.target.name]: true })
}

export function toggleFalse(e) {
  this.setState({ [e.target.name]: false })
}
