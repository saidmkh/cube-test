import React from 'react'
import { Route, Redirect } from 'react-router-dom'

export default function PrivateRoute({ component: Component, Auth, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        Auth ? <Component {...props} /> : <Redirect to="/login/" />
      }
    />
  )
}
