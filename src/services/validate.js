import isEmpty from './isEmpty'

export const userValidate = values => {
  let errors = {}

  values.name = !isEmpty(values.name) ? values.name : ''
  values.email = !isEmpty(values.email) ? values.email : ''
  values.password = !isEmpty(values.password) ? values.password : ''

  if (values.name) {
    if (values.name.length > 25) {
      errors.name = 'Must be 25 characters or less'
    } else if (values.name.length < 2) {
      errors.name = 'Must be 3 characters or more'
    }
  }

  if (!values.email) {
    errors.email = 'Email Address Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address'
  }

  if (values.password) {
    if (!values.password) {
      errors.password = 'Password Required'
    } else if (values.password.length > 30) {
      errors.password = 'Must be 30 characters or less'
    } else if (values.password.length < 6) {
      errors.password = 'Must be 7 characters or more'
    }
  }

  return {
    errors,
    validate: isEmpty(errors)
  }
}

export const postValidate = values => {
  let errors = {}

  values.title = !isEmpty(values.title) ? values.title : ''
  values.description = !isEmpty(values.description) ? values.description : ''
  if (!values.title) {
    errors.title = 'Title is Required'
  } else if (values.title.length < 2) {
    errors.title = 'Must be 2 characters or more'
  }

  if (!values.description) {
    errors.description = 'Description is Required'
  } else if (values.description.length < 2) {
    errors.description = 'Must be 2 characters or more'
  }

  return {
    errors,
    validate: isEmpty(errors)
  }
}

export const commentValidate = values => {
  let errors = {}

  values.content = !isEmpty(values.content) ? values.content : ''

  if (!values.content) {
    errors.content = 'This field is Required'
  } else if (values.content.length > 250) {
    errors.content = 'Must be 250 characters max'
  }

  return {
    errors,
    validate: isEmpty(errors)
  }
}

export const chatValidate = values => {
  let errors = {}

  values.message = !isEmpty(values.message) ? values.message : ''

  if (!values.message) {
    errors.message = 'This field is Required'
  } else if (values.message.length > 250) {
    errors.message = 'Must be 250 characters max'
  }

  return {
    errors,
    validate: isEmpty(errors)
  }
}
