import { styled } from '@material-ui/styles'

export const Container = styled(div)({
  maxWidth: '1400px',
  width: '100%',
  margin: '0 auto'
})

export const AuthContainer = styled(div)({
  margin: 'auto'
})
